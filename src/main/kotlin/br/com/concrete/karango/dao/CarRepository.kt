package br.com.concrete.karango.dao

import br.com.concrete.karango.model.Car
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CarRepository : CrudRepository<Car, Long>
