package br.com.concrete.karango.endpoint

import br.com.concrete.karango.model.Car
import br.com.concrete.karango.service.CarService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("cars")
class CarEndpoint @Autowired constructor(private val service: CarService) {

    @PostMapping
    fun save(@RequestBody car: Car): ResponseEntity<Car> {
        val createdCar = service.create(car)
        return ResponseEntity.ok(createdCar)
    }

    @GetMapping
    fun listAll(): ResponseEntity<List<Car>> {
        val allCars = service.all()
        return ResponseEntity.ok(allCars)
    }

    @PutMapping("{id}")
    fun update(@PathVariable id: Long, @RequestBody car: Car): ResponseEntity<Car> {
        if (service.existsById(id)) {
            val updatedCar = service.update(id, car)
            return ResponseEntity.ok(updatedCar)
        }
        return ResponseEntity.notFound().build()
    }

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: Long): ResponseEntity<Unit> {
        if (service.existsById(id)) {
            service.delete(id)
            return ResponseEntity.noContent().build()
        }
        return ResponseEntity.notFound().build()
    }

}
