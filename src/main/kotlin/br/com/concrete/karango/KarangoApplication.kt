package br.com.concrete.karango

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class KarangoApplication

fun main(args: Array<String>) {
    runApplication<KarangoApplication>(*args)
}
