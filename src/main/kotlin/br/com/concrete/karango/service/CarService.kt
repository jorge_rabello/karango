package br.com.concrete.karango.service

import br.com.concrete.karango.dao.CarRepository
import br.com.concrete.karango.model.Car
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CarService @Autowired constructor(private val repository: CarRepository) {

    fun all(): List<Car> = repository.findAll().toList()

    fun create(car: Car): Car = repository.save(car)

    fun update(id: Long, car: Car): Car {
        val safeCar = car.copy(id = id)
        return create(safeCar)
    }

    fun delete(id: Long) = repository.deleteById(id)

    fun existsById(id: Long) = repository.existsById(id)
}
