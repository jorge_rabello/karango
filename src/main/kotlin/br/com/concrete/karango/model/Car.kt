package br.com.concrete.karango.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonProperty.Access
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Car(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(value = "id", access = Access.READ_ONLY)
    val id: Long = 0L,
    val marca: String = "",
    val modelo: String = "",
    val placa: String = "",
    val cor: String = ""
)
