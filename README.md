# Karango

Esta é apenas uma simples demonstracão de uma aplicacão Kotlin utilizando Springboot

Fiz um CRUD simples apenas para demonstrar o uso e vou incrementar posteriormente

## Uso
No diretório raiz você vai encontrar um docker-compose.yml.

## Para inicializar o container docker do MySQL:

```
$ docker-compose up -d 
```

## Para parar o container docker do MySQL:

```
$ docker-compose down 
```

## Para realizar um build desse projeto

```
$  ./gradlew clean build
```

### OBS: 
No diretório docs estão as collections do postman para que seja possível testar o CRUD

#### Incoming
Adicionar o swagger e tratar erros da API
